FROM nvcr.io/nvidia/tritonserver:23.12-py3

RUN mkdir /model_binaries
WORKDIR /app

RUN pip install clearml torch transformers peft

COPY pull_models.py /app/pull_models.py
COPY triton/model_repository /models
COPY clearml.conf /root/clearml.conf
RUN clearml-init --file /root/clearml.conf

# Скачивание llama в образ
RUN python3 /app/pull_models.py \
    --project ATOM \
    --model_name Llama-2-7B-fp16 \
    --output_dir model_binaries
# Скачивание адаптера в образ
RUN python3 /app/pull_models.py \
    --project ATOM \
    --model_name saiga2_7b_lora \
    --output_dir model_binaries

LABEL clearml_model_id=$MODEL_ID
LABEL clearml_model_tag=$MODEL_TAG

ENTRYPOINT tritonserver --model-control-mode=poll --model-repository=/models --repository-poll-secs=60 --metrics-port=8002 --allow-metrics=true --allow-gpu-metrics=true
