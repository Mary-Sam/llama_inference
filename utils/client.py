import argparse
import time
import numpy as np

from tritonclient.utils import *
import tritonclient.http as httpclient


if __name__=='__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-i',
        '--input_text',
        type=str,
        help='Input for model',
    )
    parser.add_argument(
        '-u',
        '--url',
        type=str,
        help='URL endpoint of model.',
    )
    args = parser.parse_args()
    responses = []
    tm1 = time.perf_counter()
    with httpclient.InferenceServerClient(url=f'{args.url}', verbose=False, concurrency=32) as client:

        text_obj = np.array(args.input_text, dtype='object')

        inputs = [
            httpclient.InferInput('prompt', text_obj.shape, np_to_triton_dtype(text_obj.dtype)).set_data_from_numpy(text_obj),
        ]

        outputs = [
            httpclient.InferRequestedOutput('generated_text'),
        ]
        
        response = client.async_infer('llama_7b_with_saiga2_7b_lora', model_version='1', inputs=inputs, outputs=outputs)

        result = response.get_result()
        content = result.as_numpy('generated_text')
        print(content)
        
    tm2 = time.perf_counter()
    print(f'Total time elapsed: {tm2-tm1:0.2f} seconds')
