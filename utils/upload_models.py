import argparse
from clearml import OutputModel, Task


if __name__=='__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-m',
        '--model_name',
        type=str,
        help='Name for model that will be uploaded to ModelRegestry in ClearML.',
    )
    parser.add_argument(
        '-pn',
        '--project_name',
        type=str,
        help='Project name in ClearMl.',
    )
    parser.add_argument(
        '-p',
        '--path_weights',
        type=str,
        help='Path to model weights.',
    )
    args = parser.parse_args()
    task = Task.init(project_name=f'{args.project_name}', task_name=f'model uploading: {args.model_name}', task_type='custom')
    model = OutputModel(task=task, name=args.model_name)
    model.update_weights_package(weights_path=args.path_weights,
                                 upload_uri='https://files.clear.ml',
                                 auto_delete_file=False)
    task.close()
