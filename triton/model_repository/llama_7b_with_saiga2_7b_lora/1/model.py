import numpy as np

import triton_python_backend_utils as pb_utils
import torch
from transformers import AutoModelForCausalLM, AutoTokenizer, GenerationConfig
from peft import PeftModel, PeftConfig

MODEL_NAME = '/model_binaries/saiga2_7b_lora'
DEFAULT_MESSAGE_TEMPLATE = '<s>{role}\n{content}</s>\n'
DEFAULT_SYSTEM_PROMPT = 'Ты — Сайга, русскоязычный автоматический ассистент. Ты разговариваешь с людьми и помогаешь им.'


class Conversation:
    def __init__(
        self,
        message_template=DEFAULT_MESSAGE_TEMPLATE,
        system_prompt=DEFAULT_SYSTEM_PROMPT,
        start_token_id=1,
        bot_token_id=9225
    ):
        self.message_template = message_template
        self.start_token_id = start_token_id
        self.bot_token_id = bot_token_id
        self.messages = [{
            'role': 'system',
            'content': system_prompt
        }]

    def get_start_token_id(self):
        return self.start_token_id

    def get_bot_token_id(self):
        return self.bot_token_id

    def add_user_message(self, message):
        self.messages.append({
            'role': 'user',
            'content': message
        })

    def add_bot_message(self, message):
        self.messages.append({
            'role': 'bot',
            'content': message
        })

    def get_prompt(self, tokenizer):
        final_text = ''
        for message in self.messages:
            message_text = self.message_template.format(**message)
            final_text += message_text
        final_text += tokenizer.decode([self.start_token_id, self.bot_token_id])
        return final_text.strip()


class TritonPythonModel:
    def initialize(self, args):
        config = PeftConfig.from_pretrained(MODEL_NAME)
        base_model = AutoModelForCausalLM.from_pretrained(
            config.base_model_name_or_path,
            torch_dtype=torch.float16,
            load_in_8bit=True,
            device_map='auto'
        )
        self.model = PeftModel.from_pretrained(
            base_model,
            MODEL_NAME,
            torch_dtype=torch.float16
        )
        self.model.eval()
        self.tokenizer = AutoTokenizer.from_pretrained(MODEL_NAME, use_fast=False)
        self.generation_config = GenerationConfig.from_pretrained(MODEL_NAME)

    def generate(self, prompt):
        data = self.tokenizer(prompt, return_tensors='pt')
        data = {k: v.to(self.model.device) for k, v in data.items()}
        output_ids = self.model.generate(
            **data,
            generation_config=self.generation_config
        )[0]
        output_ids = output_ids[len(data['input_ids'][0]):]
        output = self.tokenizer.decode(output_ids, skip_special_tokens=True)
        return output.strip()   

    def execute(self, requests):
        responses = []
        for request in requests:
            # Decode the Byte Tensor into Text 
            inputs = pb_utils.get_input_tensor_by_name(request, 'prompt')
            inputs = inputs.as_numpy()
            
            # Call the Model pipeline
            conversation = Conversation()
            conversation.add_user_message(inputs)
            prompt = conversation.get_prompt(self.tokenizer)
            output = self.generate(prompt)
                        
            # Encode the text to byte tensor to send back
            inference_response = pb_utils.InferenceResponse(
            output_tensors=[
                pb_utils.Tensor(
                    'generated_text',
                    np.array([[o.encode() for o in output]]),
                    )
            ]
            )
            responses.append(inference_response)
        
        return responses

    def finalize(self, args):
        self.generator = None