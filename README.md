# Triton Inference Server for Llama_7b + LORA adapter

## ModelRegistry

Для хранения моделей используется ClearML. Для просмотра ModelRegistry необходимо написать мне (beautiful.wolf.fury@gmail.com), чтобы я добавила Вас в свое пространство ClearMl.

На текущий момент модели хранятся на стороне файл-сервера ClearMl. 

### ClearML Conf

Для инициализации ClearML в докер-образе необходимо в `clearml.conf` вставить креды следующего вида:

```
api {
    # Notice: 'host' is the api server (default port 8008), not the web server.
    api_server: https://api.clear.ml
    web_server: https://app.clear.ml
    files_server: https://files.clear.ml
    # Credentials are generated using the webapp, https://app.clear.ml/settings
    # Override with os environment: CLEARML_API_ACCESS_KEY / CLEARML_API_SECRET_KEY
    credentials {
                 "access_key": XXX,
                 "secret_key": XXX
                }
}
```

Как получить креды можно посмотреть [здесь](https://clear.ml/docs/latest/docs/getting_started/ds/ds_first_steps/).

## Общая структура

Для запуска инференса модели llama_7b + saiga2_7b_lora adapter используется Triton Inference Server, в котором определяется класс TritonPythonModel в файле model.py. В качестве бэкенда используется python.

CI/CD имеет два стейджа:

1. **build** - собирает подготовленный докер-образ с тритон-сервером и моделью (llama + адаптер), версионирование докер-образа осуществляется на основе IID пайплайна CI/CD в gitlab;
2. **deploy** - деплой образа в k8s.

Инстанс пайплайна с помощью планировщика gitlab создается раз в месяц. При наличии новой модели адаптера в ModelRegistry ClearML необходимо в последнем пайплане вручную запустить стейджи по очереди (build -> deploy).