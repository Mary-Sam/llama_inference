import argparse
import os
import shutil
from pathlib import Path

import clearml


def download_model() -> None:
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-p',
        '--project',
        type=str,
        help='Project name in ClearML.',
    )
    parser.add_argument(
        '-m',
        '--model_name',
        type=str,
        help='Model name in ClearML.',
    )
    parser.add_argument(
        '-o',
        '--output_dir',
        type=str,
        help='Output directory for model.',
    )
    args = parser.parse_args()
    in_model = clearml.InputModel(
        name=f'{args.model_name}',
        project=args.project,
    )
    print(in_model.name)
    weights_path = in_model.get_weights_package(return_path=True, raise_on_error=True)
    print('weights_path', weights_path)

    if in_model.tags == []:
        raise Exception('No tags were specified. Terminating')
    else:
        print(f'Downloaded weights to {args.output_dir}/{args.model_name}')
        if os.path.exists(args.output_dir):
            shutil.rmtree(args.output_dir)
        os.makedirs(args.output_dir, exist_ok=False)
        shutil.copytree(weights_path, f'{args.output_dir}/{args.model_name}', dirs_exist_ok=True)
        shutil.rmtree(Path(weights_path).parent)
        print(f'Weights moved to {args.output_dir}')
        os.environ['MODEL_ID'] = in_model.id
        os.environ['MODEL_TAG'] = in_model.tags[0]


if __name__ == '__main__':
    download_model()
